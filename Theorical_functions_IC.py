import matplotlib.pyplot as plt
import matplotlib.lines as lines
import math
from decimal import *
from scipy.optimize import root_scalar
from scipy.stats import binom
from scipy.special import comb
from scipy.special import logsumexp
import numpy as np
from math import sqrt,exp, log,ceil,floor

from numba import jit, njit, generated_jit, types, vectorize
























def prod_delta():
    a=[]
    i=0
    pas=0.05
    while i <= 5.1:
        if 1<=i<2:
            pas=0.1
        elif i>=2:
            pas=0.5

        a.append([round(i,2),0])
        i+=pas
    a[0][1]=0.99    
    a.sort(reverse=True)
    return a  


getcontext().prec = 200 # to allow higher precision for decimals







#produce the epsilon given by the the amplification bound (bennett)
def eps_algo(tau,n,d, p,delta):
    """
        Tau: number of target
        n: scramblers' buffer size
        p: probability of lying
        d_baseline: number of dummies
        return: epsilon_algo
    """

    rr = RRMechanism(k=tau)
    eps0=eps_baseline(tau,0, p)
    eps=BennettExactDummies(rr, d=d).get_eps(eps0, n, delta)

    return eps


#produce the delta given by the the amplification bound (bennett)
def delta_algo(tau,n,d, p,eps):
    """
        Tau: number of target
        n: scramblers' buffer size
        p: probability of lying
        d_baseline: number of dummies
        return: epsilon_algo
    """

    rr = RRMechanism(k=tau)
    eps0=eps_baseline(tau,0, p)
    eps=BennettExactDummies(rr, d=d).get_delta(eps, eps0, n)

    return eps

#produce the epsilon given by our formula
def eps_algo_exact_formula(tau, n, d, p):
    A = 1 - p+(p / (tau ))
    B = p / (tau )
    log_terms_top = np.zeros(d + 1)
    log_terms_bottom = np.zeros(d + 1)
    for k in range(d + 1):
        # use exact=True and math.log to make sure we can handle very large
        # numbers
        log_common_part = (log(comb(d, k, exact=True)) +
                           log(comb(n - 1, k, exact=True)) +
                           k * log(A) + (n - k - 1) * log(B))
        log_terms_top[k] = log_common_part + log(A + k * B * B / A)
        log_terms_bottom[k] = log_common_part + log(B + k * B * B / A)

    # use logsumexp to avoid overflows
    log_top = logsumexp(log_terms_top)
    log_bottom = logsumexp(log_terms_bottom)
    eps = log_top - log_bottom
    if eps < 0:
        eps = -eps
    return eps









#generate the delta structure produced by the empirical estimation
def generate_delta_Semi_TH(tau,Plie,n,d,nb_runs=1000):
    delta=prod_delta()
    print("")
    print("computing semi-theoricial delta")
    for i in range(len(delta)):
        delta[i][1]=get_delta_Semi_TH(tau,Plie,n,d,delta[i][0],nb_runs)
    return delta












#useful functions to get epsilon from Balle 
class ShuffleAmplificationBound:
    """Base class for "privacy amplification by shuffling" bounds."""

    def __init__(self, name='BoundBase', tol=None):
        """Parameters:
            name (str): Name of the bound
            tol (float): Error tolerance for optimization routines
        """
        self.name = name
        # Set up a default tolerance for optimization even if none is specified
        if tol is None:
            self.tol_opt = 1e-12
        else:
            self.tol_opt = tol
        # Tolerance for delta must be larger than optimization tolerance
        self.tol_delta = 10*self.tol_opt

    def get_name(self, with_mech=True):
        return self.name

    def get_delta(self, eps, eps0, n):
        """This function returns delta after shuffling for given parameters:
            eps (float): Target epsilon after shuffling
            eps0 (float): Local DP guarantee of the mechanism being shuffled
            n (int): Number of randomizers being shuffled
        """
        raise NotImplementedError

    def threshold_delta(self, delta):
        """Truncates delta to reasonable parameters to avoid numerical artifacts"""
        # The ordering of the arguments is important to make sure NaN's are propagated
        return min(max(delta, self.tol_delta), 1.0)




class NumericShuffleAmplificationBound(ShuffleAmplificationBound):
    """Base class for amplification bounds that are given in implicit form:
    F(eps,n,mechanism) <= delta
    This class implements the numerics necessary to recover eps and eps0 from implicit bounds.
    """

    def __init__(self, mechanism, name, tol=None):
        """Numeric bounds depend on properties of the mechanism"""
        super(NumericShuffleAmplificationBound, self).__init__(name=name, tol=tol)
        self.mechanism = mechanism

    def get_name(self, with_mech=True):
        if with_mech:
            return '{}, {}'.format(self.name, self.mechanism.get_name())
        return self.name

    def get_delta(self, eps, eps0, n):
        """Getting delta is bound dependent"""
        raise NotImplementedError

    def get_eps(self, eps0, n, delta, min_eps=1e-6):
        """Find the minimum eps giving <= delta"""

        assert eps0 >= min_eps
        # If this assert fails consider decreasing min_eps
        assert self.get_delta(min_eps, eps0, n) >= delta

        def f(x):
            return self.get_delta(x, eps0, n) - delta

        # Use numeric root finding
        sol = root_scalar(f, bracket=[min_eps, eps0], xtol=self.tol_opt)

        assert sol.converged
        eps = sol.root

        return eps

    def get_eps0(self, eps, n, delta, max_eps0=30):
        """Find the maximum eps0 giving <= delta"""

        assert eps <= max_eps0
        # If this assert fails consider increasing max_eps0
        assert self.get_delta(eps, max_eps0, n) >= delta

        def f(x):
            current_delta = self.get_delta(eps, x, n)
            return current_delta - delta

        # Use numeric root finding
        sol = root_scalar(f, bracket=[eps, max_eps0], xtol=self.tol_opt)

        assert sol.converged
        eps0 = sol.root

        return eps0



class BennettExactDummies(NumericShuffleAmplificationBound):
    """Numeric amplification bound based on Bennett's inequality with
    dummies """

    def __init__(self, mechanism, d=0, tol=None):
        name='Bennett with ' + str(d) + ' dummies'
        super(BennettExactDummies, self).__init__(mechanism, name, tol=tol)
        self.d = d

    def get_delta(self, eps, eps0, n):

        if eps >= eps0:
            return self.tol_delta

        self.mechanism.set_eps0(eps0)

        gamma_lb, gamma_ub = self.mechanism.get_gamma()
        a = exp(eps) - 1
        b_plus = self.mechanism.get_max_l(eps)
        c = self.mechanism.get_var_l(eps)

        alpha = c / b_plus**2
        beta = a * b_plus / c
        #eta = a / b_plus
        eta = 1.0 / b_plus

        def phi(u):
            phi = (1 + u) * log(1 + u) - u
            if phi < 0:
                # If phi < 0 (due to numerical errors), u should be small
                # enough that we can use the Taylor approximation instead.
                phi = u**2
            return phi

        exp_coef = alpha * phi(beta)
        div_coef = eta * log(1 + beta)

        def expectation_l(m):
            #coefs = np.divide(np.exp(-m * exp_coef), m * div_coef)
            coefs = (np.divide(m, m+self.d) * np.exp(-(m + self.d) * exp_coef)
                     / div_coef)
            return coefs

        delta = 1 / (gamma_lb * n)
        expectation_term = binom.expect(expectation_l, args=(n, gamma_lb), lb=1, tolerance=self.tol_opt, maxcount=100000)
        delta *= expectation_term

        return self.threshold_delta(delta)

class LDPMechanism:
    """Base class implementing parameter computations for a generic Local Randomizer.
    For now we only support randomizers satisfying pure differential privacy.
    """

    def __init__(self, eps0=1, name='Generic'):
        """Parameters:
        eps0 (float): Privacy parameter
        name (str): Randomizer's name
        """
        self.eps0 = eps0
        self.name = name

    def get_name(self):
        return self.name

    def set_eps0(self, eps0):
        self.eps0 = eps0

    def get_eps0(self):
        return self.eps0

    def get_gamma(self):
        """Returns upper and lower bounds for gamma, the blanket probability of the randomizer.
        This function implements a generic bound which holds for any pure DP local randomizer.
        """
        return exp(-self.get_eps0()), 1

    def get_max_l(self, eps):
        """Returns the maximum value of the privacy amplification random variable.
        This function implements a generic bound which hold for any pure DP local randomizer.
        """
        _, gamma_ub = self.get_gamma()
        eps0 = self.get_eps0()
        return gamma_ub * exp(eps0) * (1-exp(eps-2*eps0))

    def get_range_l(self, eps):
        """Returns the range of the privacy amplification random variable.
        This function implements a generic bound which hold for any pure DP local randomizer.
        """
        _, gamma_ub = self.get_gamma()
        eps0 = self.get_eps0()
        return gamma_ub * (exp(eps)+1) * (exp(eps0)-exp(-eps0))

    def get_var_l(self, eps):
        """Returns the variance of the privacy amplification random variable.
        This function implements a generic bound which hold for any pure DP local randomizer.
        """
        gamma_lb, gamma_ub = self.get_gamma()
        eps0 = self.get_eps0()
        return gamma_ub * (exp(eps0) * (exp(2*eps)+1) - 2 * gamma_lb * exp(eps-2*eps0))


class RRMechanism(LDPMechanism):
    """Class implementing parameter computation for a k-ary randomized response mechanism
    Bounds below are specialized exact calculations for this mechanism.
    """

    def __init__(self, eps0=1, k=2, name='RR'):
        super(RRMechanism, self).__init__(eps0=eps0, name=name)
        self.k = k

    def get_name(self, with_k=True):
        name = self.name
        if with_k:
            name += '-{}'.format(self.get_k())
        return name

    def set_k(self, k):
        self.k = k

    def get_k(self):
        return self.k

    def get_gamma(self):
        k = self.get_k()
        eps0 = self.get_eps0()
        gamma = k/(exp(eps0) + k - 1)
        return gamma, gamma

    def get_max_l(self, eps):
        k = self.get_k()
        gamma, _ = self.get_gamma()
        return gamma * (1 - exp(eps)) + (1-gamma) * k

    def get_range_l(self, eps):
        k = self.get_k()
        gamma, _ = self.get_gamma()
        return (1-gamma) * k * (exp(eps)+1)

    def get_var_l(self, eps):
        k = self.get_k()
        gamma, _ = self.get_gamma()
        return gamma * (2-gamma) * (exp(eps)-1)**2 + (1-gamma)**2 * k * (exp(2*eps) + 1)


    
@njit()
def eps_sampling(tau, p):
    """
        Tau: number of target
        p: probability of lying
        d_baseline: number of dummies
        return: epsilon_baseline
    """
    top_baseline = 1 - p
    bottom_baseline = p/tau
    eps_baseline = (top_baseline / bottom_baseline)+1
    eps_baseline= log(eps_baseline)
    if eps_baseline < 0:
        eps_baseline = -eps_baseline
    return eps_baseline  
    

@njit()
#produce the epsilon given by the baseline algorithm (i.e. without scrambling)
def eps_baseline(tau, d_baseline, p):
    """
        Tau: number of target
        p: probability of lying
        d_baseline: number of dummies
        return: epsilon_baseline
    """
    top_baseline = (1 - p) 
    bottom_baseline = (p) * (d_baseline + 1)* (1/tau)
    eps_baseline = (top_baseline / bottom_baseline)+1
    eps_baseline= log(eps_baseline)
    if eps_baseline < 0:
        eps_baseline = -eps_baseline
    return eps_baseline


@njit()
# estimate the expectation of random variables Li empirically for a specific number of lyars
def estimate(tau, d, Plie, eps, m, R, error_rate=0.95):
    
    """
        k=tau
        d=dummies
        gamma=Plie
        eps=eps0
        m=nb lie
        R= nombre de tirages
    """
    x1 = 0
    x2 = 1
    L = np.zeros(R)
    for r in range(R):
        W = np.random.choice(np.arange(tau), size=m+d)
        iden = (W==x1) - np.exp(eps) * (W==x2)
        l = Plie * (1 - np.exp(eps)) + (1 - Plie) * tau * iden
        L[r] = l.sum()
        L[r] *= L[r] > 0
        
    IC=error_rate*(np.std(L)/sqrt(R))
    mean=L.mean()
    return mean,mean-IC,mean+IC





#implem en log
def get_delta_Semi_TH_log(tau,Plie,n,d,eps,nb_runs=1000, error_rate=0.95):
    

    result=0
    result_min=0
    result_max=0
    for m in range(1,n+1):
        A=log(m)-log(m+d)
        B=log(comb(n, m, exact=True))
        C=log(Plie)*m
        D=log(1-Plie)*(n-m)
        E,E_min,E_max=estimate(tau, d, Plie, eps, m, nb_runs, error_rate)
        result+=exp(A+B+C+D)*E
        result_min+=exp(A+B+C+D)*E_min
        result_max+=exp(A+B+C+D)*E_max
    F=exp(-log(Plie)-log(n))
    return result*F,result_min*F,result_max*F

#get the smalest epsilon for a given delta with the empirical estimation of Li (eps aurelien)
def get_epsilon_Semi_TH_optimized_tmp(tau,Plie,n,d,delta,nb_runs=1000, error_rate=0.95):
    EPS_MAX=eps_baseline(tau, 0, Plie)
    MAX_DICHOTOMIE=10
    
    size=2**MAX_DICHOTOMIE
    
    epsilons=np.arange(0, EPS_MAX, EPS_MAX/size)
    
    
    indice=size//2
    pas=indice//2
    mini=1
    best_indice=0
    eps_final=0
    eps_max=0
    eps_min=0
    for i in range(0,MAX_DICHOTOMIE):
        delta_tmp,delta_tmp_min,delta_tmp_max=get_delta_Semi_TH_log(tau,Plie,n,d,epsilons[indice],nb_runs, error_rate)
        if mini>abs(delta_tmp-delta):
            mini=abs(delta_tmp-delta)
            closer_delta=delta_tmp
            best_indice=indice
        if delta_tmp>delta:
            indice=indice+pas
        elif delta_tmp<delta:
            indice=indice-pas
        else :
            best_indice=indice
            break            
        pas=pas//2
    return epsilons[best_indice],delta_tmp_min,delta_tmp_max


def get_epsilon_Semi_TH_optimized(tau,Plie,n,d,delta,nb_runs=1000, error_rate=0.95):
    eps,delta_tmp_min,delta_tmp_max=get_epsilon_Semi_TH_optimized_tmp(tau,Plie,n,d,delta,nb_runs, error_rate)
    MAX_DICHOTOMIE=10
    eps_min=100
    eps_max=0

    """
    eps_min,_,_=get_epsilon_Semi_TH_optimized_tmp(tau,Plie,n,d,delta_tmp_max,nb_runs, error_rate)
    if eps_min>eps:
        eps_min=eps-eps*0.1
    """
    eps_max,_,_=get_epsilon_Semi_TH_optimized_tmp(tau,Plie,n,d,delta_tmp_min,nb_runs, error_rate)
    if eps_max<eps:
        eps_max=eps+eps*0.1
    
    eps_min = eps-(eps_max-eps)
    return eps,eps_min,eps_max

delta=0.0001
tau=5
Plie=0.1
n=1
d=0
nb_runs=1
eps=1
m=1
estimate(tau, d, Plie, eps, m, nb_runs)
get_delta_Semi_TH_log(tau,Plie,n,d,eps,nb_runs)
get_epsilon_Semi_TH_optimized(tau,Plie,n,d,eps,nb_runs)




def get_n_TH_optimized(tau,Plie,eps,d,delta):
    N_MAX=10240
    MAX_DICHOTOMIE=10
    
    size=2**MAX_DICHOTOMIE
    
    ns=np.arange(0, N_MAX, int(N_MAX/size))
    
    indice=size//2
    pas=indice//2
    mini=1
    for i in range(0,MAX_DICHOTOMIE):
        delta_tmp=delta_algo(tau,ns[indice],d, Plie,eps)
        if mini>abs(delta_tmp-delta):
            mini=abs(delta_tmp-delta)
            closer_delta=delta_tmp
            best_indice=indice
        if delta_tmp>delta:
            indice=indice+pas
        elif delta_tmp<delta:
            indice=indice-pas
        else :
            return ns[indice]
        pas=pas//2
    return ns[best_indice]




def get_n_Semi_TH_optimized(tau,Plie,eps,d,delta,nb_runs=1000):
    N_MAX=5120
    MAX_DICHOTOMIE=9
    
    size=2**MAX_DICHOTOMIE
    
    ns=np.arange(0, N_MAX, int(N_MAX/size))
    
    indice=size//2
    pas=indice//2
    mini=1
    for i in range(0,MAX_DICHOTOMIE):
        delta_tmp=get_delta_Semi_TH_log(tau,Plie,ns[indice],d,eps,nb_runs)
        if mini>abs(delta_tmp-delta):
            mini=abs(delta_tmp-delta)
            closer_delta=delta_tmp
            best_indice=indice
        if delta_tmp>delta:
            indice=indice+pas
        elif delta_tmp<delta:
            indice=indice-pas
        else :
            return ns[indice]
        pas=pas//2
    return ns[best_indice]

