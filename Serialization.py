from math import ceil,log, inf
from random import randint, seed, random
import numpy as np
import matplotlib.pyplot as plt
import os.path
from os import path
from csv import writer,reader
import shutil
import pickle



def do_file_exists(filename):
    """
        teste si un fichier existe ou pas, prend en paramètre le nom ou le path du fichier
    """
    if os.path.isfile(filename):
        return True
    else:
        return False

       
    

def do_storage_exists(filename):
    """
        teste si un fichier existe ou pas, prend en paramètre le nom ou le path du fichier
    """
    filename="Storage/"+filename+".txt"
    if os.path.isfile(filename):
        return True
    else:
        return False

         
def store(name, obj):
    name="Storage/"+name+".txt"
    with open(name, "wb") as fp:
        pickle.dump(obj, fp)
        
def restore(name):
    name="Storage/"+name+".txt"
    with open(name, "rb") as fp:
        return pickle.load(fp)    
    
    
    
def saveFig(name):
    plt.savefig("Curves/"+name+".pdf")  
    plt.savefig("Curves/"+name+".png") 
    
    
def saveFig2(name):
    plt.rcParams.update({'font.size': 14})
    plt.gcf().subplots_adjust(bottom=0.17)
    plt.savefig("Final/"+name+".pdf",bbox_inches='tight')  
    plt.savefig("Final/"+name+".png",bbox_inches='tight')     
    

def write_dataset_into_csv(csv_filename,D):
    #ouverture du fichier en écriture
    csv_file=open(csv_filename,newline='',mode='w',encoding='utf-8')

    #definition du délimiteur
    csv_writer=writer(csv_file,delimiter=';')

    # écriture du datasets dans le fichier csv
    csv_writer.writerow(D)

    #fermuture du fichier csv (important pour la prise en compte des changement effectués)
    csv_file.close()
    

    
    
def extract_dataset_from_csv(csv_filename):
    csv_file=open(csv_filename, 'r',encoding='utf-8')
    read = reader(csv_file)


    # on parse le fichier ligne par ligne et on stocke le resultat obtenue dans une liste
    listdatasets=[]
    for row in read:
        listdatasets.append(row[0].split(";")) 

    #fermeture du fichier
    csv_file.close()
    # mettre les datasets recupérés dans des listes séparées
    D1=listdatasets[0]

    # cast des listes de string à int
    for i in range(len(D1)): 
        D1[i] = int(D1[i]) 

    return D1 


def write_output_into_csv(csv_filename,O):
    #ouverture du fichier en écriture
    csv_file=open(csv_filename,newline='',mode='w',encoding='utf-8')

    #definition du délimiteur
    csv_writer=writer(csv_file,delimiter=';')

    for i in O.keys():
        csv_writer.writerow([i,O[i]])
        

    csv_file.close()
    
    
def extract_output_from_csv(csv_filename):
    #ouverture du fichier csv en lecture
    csv_file=open(csv_filename, 'r',encoding='utf-8')

    read = reader(csv_file,delimiter=';')

    tmp=dict()
    # on parse le fichier ligne par ligne et on stocke le resultat obtenue dans une liste
  

    for row in read:
        tmp[eval(row[0])]=[int(row[1])]


        
    csv_file.close()   

    return tmp     
def write_res_into_csv(csv_filename,res):
    if path.exists('Res-structure')==False:
        os.mkdir('Res-structure')
    #ouverture du fichier en écriture
    csv_file=open(csv_filename,newline='',mode='w',encoding='utf-8')

    #definition du délimiteur
    csv_writer=writer(csv_file,delimiter=';')

    for i in res.keys():
        csv_writer.writerow([i,res[i]])
        

    csv_file.close()
    
    
def extract_res_from_csv(csv_filename):
    #ouverture du fichier csv en lecture
    csv_file=open(csv_filename, 'r',encoding='utf-8')

    read = reader(csv_file,delimiter=';')

    tmp=dict()
    # on parse le fichier ligne par ligne et on stocke le resultat obtenue dans une liste
  
    for row in read:
        val=row[1].strip('][').split(', ')
        tmp[eval(row[0])]=[int(val[0]),int(val[1])]
        
    csv_file.close()   

    return tmp       

        
def write_Epsilon_into_csv(csv_filename,Eps):
    
    if path.exists('Epsilons')==False:
        os.mkdir('Epsilons')
    csv_filename="Epsilons/"+csv_filename
    #ouverture du fichier en écriture
    csv_file=open(csv_filename,newline='',mode='w',encoding='utf-8')

    #definition du délimiteur
    csv_writer=writer(csv_file,delimiter=';')

    # écriture du datasets dans le fichier csv

    for i in range(len(Eps)):
        csv_writer.writerow(Eps[i])

    #fermuture du fichier csv (important pour la prise en compte des changement effectués)
    csv_file.close()

def delta_to_csv(csv_filename, delta):
    """
        tau: number of targets
        n,d: input size, number of dummies
        with_seed: used seed to produce inputs & outputs
        TH_XP: "TH" for theoric, "XP" for experiments
        delta: [[eps,delta]] to be placed in the csv file
    """
    #ouverture du fichier en écriture
    csv_file=open(csv_filename,newline='',mode='w',encoding='utf-8')

    #definition du délimiteur
    csv_writer=writer(csv_file,delimiter=';')

    # écriture de delta [[eps,delta]] dans le fichier csv
    for d in delta:
        csv_writer.writerow(d)

    #fermuture du fichier csv (important pour la prise en compte des changement effectués)
    csv_file.close()
    
    
def csv_to_delta(csv_filename):

    delta=[]
    # ouverture du fichier csv en lecture
    csv_file=open(csv_filename, 'r', encoding='utf-8')
    read = reader(csv_file)

    # parsing le fichier et stockage du resultat obtenue dans une liste
    couples=[]
    for row in read:
        couples.append(row[0].split(";"))
    
    # insertion de la liste dans la structure delta
    for i in range(len(couples)):
        for j in range(len(couples[i])):
            couples[i][j] = float(couples[i][j])
    csv_file.close()   
         
    return couples
            
def clear():
    shutil.rmtree('inputs', ignore_errors=True)
    shutil.rmtree('outputs', ignore_errors=True)
    shutil.rmtree('Res-structure', ignore_errors=True)
        
        


def write_birds_into_csv(csv_filename,birds):
    if path.exists('Birds')==False:
        os.mkdir('Birds')
        

    #ouverture du fichier en écriture
    csv_file=open(csv_filename,newline='',mode='w',encoding='utf-8')

    #definition du délimiteur
    csv_writer=writer(csv_file,delimiter=';')

    # écriture du datasets dans le fichier csv
    for i in birds:
        csv_writer.writerow(i)

    #fermuture du fichier csv (important pour la prise en compte des changement effectués)
    csv_file.close()
    

    
    
def extract_birds_from_csv(csv_filename):
    csv_file=open(csv_filename, 'r',encoding='utf-8')
    read = reader(csv_file)

    tmp=tuple()
    # on parse le fichier ligne par ligne et on stocke le resultat obtenue dans une liste
    birds=[]
    for row in read:      
        tmp2=row[0].split(";")
        tmp2[0]=int(tmp2[0])
        tmp2[1]=tmp2[1]
        tmp2[2]=float(tmp2[2])
        tmp2[3]=int(tmp2[3])
        birds.append(tuple(tmp2))


    #fermeture du fichier
    csv_file.close()

    return birds





#fonctions utilies pour générer les noms de fichiers csv
def gen_filename_initial_dataset(tau,n,with_seed,initial_dataset):
    return "inputs/INPUT_initial_tau_{},n_{},seed_{},initial-dataset_{}.csv".format(tau,n,with_seed,initial_dataset)

def gen_filename_neighbor_dataset(tau,n,with_seed,initial_dataset,forced_neighbors):
    return "inputs/INPUT_neighbor_tau_{},n_{},seed_{},initial-dataset_{},forced-neighbors_{}.csv".format(tau,n,with_seed,initial_dataset,forced_neighbors)

def gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number):
    return "outputs/BUNCH_initial_tau_{},plie_{},n_{},d_{},seed_{},initial-dataset_{},bunch-size_{},bunch_{}.csv".format(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)

def gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number):
    return "outputs/BUNCH_neighbor_tau_{},plie_{},n_{},d_{},seed_{},initial-dataset_{},forced-neighbors_{},bunch-size_{},bunch_{}.csv".format(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number) 


def gen_name_res(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys):
    return "Res-structure/RES_tau_{},plie_{},n_{},d_{},seed_{},iteration_{},bunch-size_{},initial-dataset_{},forced-neighbors_{},limit-Keys_{}.csv".format(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)

def gen_name_birds(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys):
    return "Birds/BIRDS_tau_{},plie_{},n_{},d_{},seed_{},iteration_{},bunch-size_{},initial-dataset_{},forced-neighbors_{},limit-Keys_{}.csv".format(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
        

def gen_name_delta(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys):
    return "deltas/DELTA_tau_{},plie_{},n_{},d_{},seed_{},iteration_{},bunch-size_{},initial-dataset_{},forced-neighbors_{},limit-Keys_{}.csv".format(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
          







