from math import ceil,log, inf
from random import randint, seed, random
import numpy as np
import matplotlib.pyplot as plt
import os.path
from os import path
from csv import writer,reader
import shutil
from Theorical_functions import *
from Serialization import *
from Ploting import *
import re

# generates a dataset
def generate_initial_dataset(tau,n,with_seed=0):
       
    """
        tau: number of targets
        n: size of the scramblers' buffer
        with_seed: if specified, allows to draw a persistant dataset
        return : a uniform dataset 
    """
    

    if with_seed > 0: seed(with_seed)

    Dataset=[0 for i in range(tau)]
    for i in range(n):
        t=randint(0, tau-1)
        Dataset[t]=Dataset[t]+1

     
    return Dataset
    
#generates a neighbor dataset for a given dataset
def generate_neighbor(D,forced_neighbor=[0,0]):
    D2=D.copy()
    tau=len(D)
    if forced_neighbor!=[0,0]:
        if D2[forced_neighbor[0]]==0:
            print("ERROR, 0 messages  are initialy targeted to target {}".format(forced_neighbor[0]))
            return -1
        D2[forced_neghibor[0]]=D2[forced_neighbor[0]]-1
        D2[forced_neghibor[1]]=D2[forced_neighbor[1]]+1
        
    else:
        while (True):
            x1=randint(0, tau-1)
            x2=randint(0, tau-1)
            if x1!=x2 and D2[x1]>0 and D2[x2]>0:
                break       
        D2[x1]=D2[x1]+1
        D2[x2]=D2[x2]-1
        
    return D2



#generates two neighboring datasets for some given parameters and store them in csv files
def generate_datasets(tau,n,with_seed=0,forced_neighbors=[0,0],initial_dataset=-1):
    
    if with_seed > 0: seed(with_seed)
    if initial_dataset!=-1:
        if len(initial_dataset)!=tau:
            print("ERROR,  size of initial dataset different from tau={}".format(tau))
            return -1,-1
    
    
    # vérification si le répertoire inputs existe sinon le créer 
    if path.exists('inputs')==False:
        os.mkdir('inputs')
        
    #nom des fichiers csv 
    csv_filename_initial=gen_filename_initial_dataset(tau,n,with_seed,initial_dataset)
    csv_filename_neighbor=gen_filename_neighbor_dataset(tau,n,with_seed,initial_dataset,forced_neighbors)
    
    
    if do_file_exists(csv_filename_initial):
        D1=extract_dataset_from_csv(csv_filename_initial)
    elif initial_dataset==-1:
        D1=generate_initial_dataset(tau,n,with_seed)
        write_dataset_into_csv(csv_filename_initial,D1)
    else:
        D1=initial_dataset.copy()
        write_dataset_into_csv(csv_filename_initial,D1)
        
    if do_file_exists(csv_filename_neighbor):
        D2=extract_dataset_from_csv(csv_filename_neighbor)
    else:
        D2=generate_neighbor(D1,forced_neighbors)
        write_dataset_into_csv(csv_filename_neighbor,D2)    
    print(D1)
    print(D2)
    return D1,D2
    
    





#initial algo: without limits
def scrambeling(D,plie,d):
    """
        D: a dataset containing the number of messages sent to each target
        Plie: probability of lying
        d: number of dummies
        O: a list of tau elements that represents the number of messages received
                by each target (gaussian dummies)
    """
    tau=len(D)
    n=sum(D)
    O=[0 for i in range(tau)]
    
    #Sample the input dataset:
    for b in range(tau):
        for i in range(D[b]):
            x = random()
            if x > plie: O[b]=O[b]+1 #send true message
            else: d=d+1 #replace true message with a dummy
                 
    # Add dummies at random in non full targets:
    for i in range(d):
        t = randint(0, tau-1)
        O[t]=O[t]+1

    return O
    
    
    
    
    
    
# fonction qui prend un dataset et exécute l'algo iterations fois puis compte le nombre de fois que chaque output est sortie
def Outputs_Genration(D,plie,d,iterations):
    """
        D: a dateset containing the number of messages sent to each target
        plie: probability of lying
        d: number of dummies
        iterations: number of iterations (default 10000)
        avoid_zero: True => retry when an output lets a given target without dummy
        O: a key-value dictionary where the key is a list of tau elements that represents the number of messages received
           by each target and value is the number of times this key appears.
    """
    O=dict()
    for i in range(iterations):
        
        tmp=tuple(scrambeling(D,plie,d))

        if tmp in O.keys():
            O[tmp]=O[tmp]+1
        else:
            O[tmp]=1
    return O    
    


    
    
    
def Generate_persistant_outputs_bunch(D1,D2,tau,plie,n,d,bunch_number,bunch_size,with_seed=0,forced_neighbors=[0,0],initial_dataset=-1):
    #nom des fichiers csv
    csv_filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
    csv_filename_neighbor=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number)
    if path.exists('outputs')==False:
        os.mkdir('outputs')
    
    O1=dict()
    O2=dict()
    if(do_file_exists(csv_filename_initial)==False):
        O1=Outputs_Genration(D1,plie,d,bunch_size)
        write_output_into_csv(csv_filename_initial,O1)    
    
    if(do_file_exists(csv_filename_neighbor)==False):
        O2=Outputs_Genration(D2,plie,d,bunch_size)
        write_output_into_csv(csv_filename_neighbor,O2) 
    
    
    
def Generate_persistant_outputs(D1,D2,tau,plie,n,d,iterations,bunch_size,with_seed=0,forced_neighbors=[0,0],initial_dataset=-1):
    
    number_of_bunches=ceil(iterations/bunch_size)
    O=dict()
    for bunch_number in range(number_of_bunches):
        print("{} bunch generated over {}".format(bunch_number+1, number_of_bunches), end="\r", flush=True)
        if with_seed > 0: seed(with_seed+bunch_number)
        Generate_persistant_outputs_bunch(D1,D2,tau,plie,n,d,bunch_number,bunch_size,with_seed,forced_neighbors,initial_dataset)
        
        
        
        
        
        
def build(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1):
    D1,D2=generate_datasets(tau,n,with_seed,forced_neighbors,initial_dataset)
    plot_datasets(D1,D2)
    Generate_persistant_outputs(D1,D2,tau,plie,n,d,iterations,bunch_size,with_seed,forced_neighbors,initial_dataset)
        


def build_verif(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1):
    # vérification si un build a déjà été fait avec ces params
    number_of_bunches=ceil(iterations/bunch_size)
    for bunch_number in range(0,number_of_bunches):
        
        filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
        filename_neighbor=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number) 
        
        print("{} bunch vérified over {}".format(bunch_number+1, number_of_bunches), end="\r", flush=True)
        
        if(do_file_exists(filename_initial)==False or do_file_exists(filename_neighbor)==False):
            return 0
        
    return 1

def extract_res_structure(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1):

    
    #NA:
    birds = [] #to store [ [id_bunch, new_occ, past_occ] ... ] 
    bunch_id = 1
    
    #initialisation
    res=dict()
    number_of_keys=0
    number_of_bunches=ceil(iterations/bunch_size)
    number_of_actual_output_D1=0
    number_of_actual_output_D2=0
    number_of_outputs=0
    starting_bunch=0
    
    
    #création des noms de fichiers
    res_filename= gen_name_res(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
    birds_filename=gen_name_birds(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
    
    #vérification si les fichiers existent déjà
    if do_file_exists(res_filename):
        #print("Extracting Res structure, this may take a while !",end="\r", flush=True)
        res=extract_res_from_csv(res_filename)
        if do_file_exists(birds_filename):
            #print("Extracting Birds structure, this may take a while !",end="\r", flush=True)
            birds=extract_birds_from_csv(birds_filename)
        else:
            birds=[]
        return res, birds
    
    # vérification si un build a déjà été fait avec ces params     
    if build_verif(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1)==0:
        print("ERROR: a built with these parameters must be done before !!")
        return -1

        
    #début des calculs    
    for bunch_number in range(starting_bunch,number_of_bunches):
        filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
        filename_neighbor=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number)
        
        print("{} bunch processed over {}".format(bunch_number+1, number_of_bunches), end="\r", flush=True)
        tmp1=extract_output_from_csv(filename_initial)
        tmp2=extract_output_from_csv(filename_neighbor)



        #NA:
        outliers = 0


        for i in tmp1.keys():
            #if number_of_outputs>=iterations: break
            if i in res.keys():
                if res[i][0]==0:
                    outliers = outliers + tmp1[i][0]
                res[i]=[res[i][0]+tmp1[i][0],res[i][1]]
                number_of_outputs=number_of_outputs+tmp1[i][0]


            elif number_of_keys<limit_keys or limit_keys==-1:
                res[i]=[tmp1[i][0],0]

                number_of_keys=number_of_keys+1
                number_of_outputs=number_of_outputs+tmp1[i][0]

                #NA:
                outliers = outliers + tmp1[i][0]


        #NA:
        birds.append((
            bunch_id,"D1",                  #bunch_id,#dataset_id
            float(outliers)/(bunch_size),       #occ_new 
            (bunch_id)*bunch_size))  #occ_old
        outliers = 0


        for i in tmp2.keys():
            #if number_of_outputs>=iterations: break
            if i in res.keys():
                if res[i][1]==0:
                    outliers = outliers + tmp2[i][0]
                res[i]=[res[i][0],res[i][1]+tmp2[i][0]]
                number_of_outputs=number_of_outputs+tmp2[i][0]



            elif number_of_keys<limit_keys or limit_keys==-1:
                res[i]=[0,tmp2[i][0]]
                number_of_keys=number_of_keys+1
                number_of_outputs=number_of_outputs+tmp2[i][0]
                #NA:
                outliers = outliers + tmp2[i][0]

        #NA:
        birds.append((
            bunch_id,"D2",                  #bunch_id,#dataset_id
            float(outliers)/(bunch_size),       #occ_new 
            (bunch_id)*bunch_size))  #occ_old
        bunch_id = bunch_id + 1
        outliers = 0
        #print("Len de res",len(res.keys()))
                 
    #NA: 

   
  
    
    write_res_into_csv(res_filename,res)
    write_birds_into_csv(birds_filename,birds)
    
    return res,birds
                         
        
def generate_epsilon(Res):
    tmp = list(Res.values())
    total=[sum(x) for x in zip(*tmp)] 
    total_outputs_D1=total[0]
    total_outputs_D2=total[1]
    #print("")
    #print("Generating Epsilon...")
    Total_outputs=total_outputs_D1+total_outputs_D2

    Eps=[]# [eps, occ_1, occ_2,output]
    for i in Res.keys():

        if Res[i][0]==0 or Res[i][1]==0:
            eps=inf
        else:
            ratio=Res[i][0]/Res[i][1]
            eps=max(log(ratio),log(1/ratio))
        Eps.append([eps,Res[i][0],Res[i][1],i])

    
    Eps.sort(reverse=True)
            
        
    return Eps

                                 
def prod_delta():
    a=[]
    i=0
    pas=0.005
    while i <= 5.1:
       

        a.append([round(i,3),0])
        i+=pas
    a[0][1]=1    
    a.sort(reverse=True)
    return a                                           
def experiments(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1):

    if path.exists('deltas')==False:
        os.mkdir('deltas')
    
    csv_filename=gen_name_delta(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
    birds_filename=gen_name_birds(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
    """
    if do_file_exists(csv_filename):
        print("Files already exists, wait while extracting results !", end="\r", flush=True)
        delta=csv_to_delta(csv_filename)
        birds=extract_birds_from_csv(birds_filename)
        return delta, birds
    """
    Res, birds= extract_res_structure(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors,initial_dataset,limit_keys)
    Eps=generate_epsilon(Res)
    #todo copute the same only for occ_D1
    produced_outputs=0
    for i in Eps: #epsilon, occ_D1, occ_D2, output
        produced_outputs = i[1]+i[2] + produced_outputs


    # compute delta for outputs sorted by epsilon
    #delta=[(eps,delta)] avec eps=10,5,2,1,0.9,...,0.1,0
    delta=prod_delta()
    cum_proba=0.0
    current_eps=0




    for i in Eps: #i=[epsilon, occ_D1, occ_D2, output]
        if i[0]<delta[current_eps][0]: #if i.epsilon < current delta.epsilon
            delta[current_eps][1]=cum_proba # write cumulated delta for current epsilon
            current_eps=current_eps+1 #go to next epsilon interval (always exists)
        cum_proba=(i[1]+i[2])/(produced_outputs) + cum_proba #cumulate proba (occ_1+occ_2)/produced_output


    plot_epsilon(Eps)
    plot_epsilon2(Eps)
    delta_to_csv(csv_filename, delta)
    
    
    
    return delta, birds



def extract_birds(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1):

    
    #NA:
    birds = [] #to store [ [id_bunch, new_occ, past_occ] ... ] 
    bunch_id = 1
    
    
    res=dict()
    number_of_keys=0
    number_of_bunches=ceil(iterations/bunch_size)
    number_of_actual_output_D1=0
    number_of_actual_output_D2=0
    number_of_outputs=0
    starting_bunch=0
                                           
    birds_filename=gen_name_birds(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)
    
                                           

    
    for bunch_number in range(starting_bunch,number_of_bunches):
        if(bunch_number%50000==0):
            print("")
        filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
        filename_neighbor=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number) 
        print("{} bunches verified over {}".format(bunch_number+1, number_of_bunches), end="\r", flush=True)
        
        if(do_file_exists(filename_initial)==False or do_file_exists(filename_neighbor)==False):
            print(filename_initial)
            print(filename_neighbor)
            print("ERROR: a built with these parameters must be done before !!")
            return -1
    print("Files verification done succerssfully, starting birds generation")
    
    for bunch_number in range(starting_bunch,number_of_bunches):
        filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
        filename_neighbor=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number)
        if(bunch_number%50000==0):
            print("")
        print("{} bunches processed over {}".format(bunch_number+1, number_of_bunches), end="\r", flush=True)
        
        tmp1=extract_output_from_csv(filename_initial)
        tmp2=extract_output_from_csv(filename_neighbor)



        #NA:
        outliers = 0


        for i in tmp1.keys():
            #if number_of_outputs>=iterations: break
            if i in res.keys():
                if res[i][0]==0:
                    outliers = outliers + tmp1[i][0]
                res[i]=[res[i][0]+tmp1[i][0],res[i][1]]
                number_of_outputs=number_of_outputs+tmp1[i][0]


            elif number_of_keys<limit_keys or limit_keys==-1:
                res[i]=[tmp1[i][0],0]

                number_of_keys=number_of_keys+1
                number_of_outputs=number_of_outputs+tmp1[i][0]

                #NA:
                outliers = outliers + tmp1[i][0]


        #NA:
        birds.append((
            bunch_id,"D1",                  #bunch_id,#dataset_id
            float(outliers)/(bunch_size),       #occ_new 
            (bunch_id)*bunch_size))  #occ_old
        outliers = 0


        for i in tmp2.keys():
            #if number_of_outputs>=iterations: break
            if i in res.keys():
                if res[i][1]==0:
                    outliers = outliers + tmp2[i][0]
                res[i]=[res[i][0],res[i][1]+tmp2[i][0]]
                number_of_outputs=number_of_outputs+tmp2[i][0]



            elif number_of_keys<limit_keys or limit_keys==-1:
                res[i]=[0,tmp2[i][0]]
                number_of_keys=number_of_keys+1
                number_of_outputs=number_of_outputs+tmp2[i][0]
                #NA:
                outliers = outliers + tmp2[i][0]

        #NA:
        birds.append((
            bunch_id,"D2",                  #bunch_id,#dataset_id
            float(outliers)/(bunch_size),       #occ_new 
            (bunch_id)*bunch_size))  #occ_old
        bunch_id = bunch_id + 1
        outliers = 0
        #print("Len de res",len(res.keys()))
                                           
        tmp_cmp=bunch_size*(bunch_number+1)                                   
        birds_filename_tmp=gen_name_birds(tau,plie,n,d,with_seed,tmp_cmp,bunch_size,initial_dataset,forced_neighbors,limit_keys) 
        if bunch_number%1000==0:
            write_birds_into_csv(birds_filename_tmp,birds)
    #NA: 

   
  
    
    write_birds_into_csv(birds_filename,birds)
    
    return birds   

def gen_birds(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1):

    if path.exists('deltas')==False:
        os.mkdir('deltas')
    
    birds_filename=gen_name_birds(tau,plie,n,d,with_seed,iterations,bunch_size,initial_dataset,forced_neighbors,limit_keys)

    if do_file_exists(birds_filename):
        print("Files already exists, wait while extracting results !")
        birds=extract_birds_from_csv(birds_filename)
        return birds

    birds= extract_birds(tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors,initial_dataset,limit_keys)
    
    
    return birds    
    
def built_stat():
    if path.exists('outputs')==False:
        os.mkdir('outputs')
    T=os.listdir('outputs/')
    if len(T)==0:
        print("No build has been done so far !!")

    T = [t for t in T if not t.startswith('BUNCH_initial')]

    for i in range(len(T)):
        while T[i][-1]!='h':
            T[i]=T[i][:-1]

    T=[i[15:-6] for i in T]
    tmp=T.copy()

    T = list(dict.fromkeys(T))

    for i in range(len(T)):
        T[i]=T[i]+",iterations-max="+str(int(((T[i].split(","))[-1]).split('_')[1])*tmp.count(T[i]))
        T[i]=T[i].replace("_","=")
        print(T[i])

    
    
    
def generate_theorical_delta(tau,n,d,plie):
    delta=0.0000001
    delta_theoric=prod_delta()
    #delta=[(eps,delta)] avec eps=10,5,2,1,0.9,...,0.1,0
    e = eps_algo(tau,n,d,plie,delta)
    current_eps = 0
    while delta<=1:
        e = eps_algo(tau,n,d,plie,delta)
        while e<delta_theoric[current_eps][0]:
            delta_theoric[current_eps][1]=delta
            current_eps=current_eps+1
        if e > delta_theoric[current_eps][0]:
            delta_theoric[current_eps][1]=delta
        else: 
            current_eps=current_eps+1
        delta=delta+0.001
        
    for i in range(len(delta_theoric)):
        if delta_theoric[i][1]==0:
            delta_theoric[i][1]=1
      
    return delta_theoric
    

    
    
    

    
    
    
    
    
def extract_occr_output(output,tau,plie,n,d,with_seed,iterations,bunch_size,forced_neighbors=[0,0],initial_dataset=-1,limit_keys=-1):

    
    result=dict()

    number_of_bunches=ceil(iterations/bunch_size)


    occ1=0
    occ2=0
    
    for bunch_number in range(number_of_bunches):
        filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
        filename_neighbor=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number) 

        if(do_file_exists(filename_initial)==False or do_file_exists(filename_neighbor)==False):
            
            print("ERROR: a built with these parameters must be done before !!")
            return -1
    else:
        for bunch_number in range(number_of_bunches):
            filename_initial=gen_name_init_out(tau,plie,n,d,with_seed,initial_dataset,bunch_size,bunch_number)
            filename_neigh=gen_name_neigh_out(tau,plie,n,d,with_seed,initial_dataset,forced_neighbors,bunch_size,bunch_number)
    
            tmp1=extract_output_from_csv(filename_initial)
            tmp2=extract_output_from_csv(filename_neigh)
            
            
            if output in tmp1.keys():
                occ1=occ1+tmp1[output][0]
            
            
            if output in tmp2.keys():
                occ2=occ2+tmp2[output][0]
                
            result[(bunch_number+1)*bunch_size]=[occ1,occ2]
                    
     
    
    return result    
    
    
def extract_max_iteration(csv_filename):
    csv2=csv_filename[18:]
    
    iteration=re.search('iteration_(.+?),bunch-size', csv_filename)
    iteration=int(iteration.group(1))

    T=os.listdir('Res-structure/')
    T2=[]
    T3=[]

    debut=re.search('(.+?.+)iteration_', csv2)
    debut=debut.group(1)

    fin=re.search(',bunch-size_(.+?.+)', csv2)
    fin=fin.group(1)


    #garder que les noms de fichiers qui commence
    for i in range(len(T)):
        T[i]=(T[i][4:])
        if(debut==T[i][:len(debut)]):
            T2.append(T[i][len(debut):])

    for i in range(len(T2)):
        if(fin==T2[i][len(T2[i])-len(fin):]):
            tmp=T2[i][:len(T2[i])-len(fin)]
            m = re.search('iteration_(.+?.+),bunch-size', tmp)
            if(int(m.group(1))<iteration):
                T3.append(int(m.group(1)))

    if len(T3)==0:
        return 0
    return max(T3)







