from math import ceil,log, inf
from random import randint, seed, random
import numpy as np
import matplotlib.pyplot as plt
import os.path
from os import path
from csv import writer,reader
import shutil

from Theorical_functions import *

#affiche un ou 2 datasets sous forme d'un graphe
def plot_datasets(D1,D2=0):
    """
        Input: 1 ou 2 datasets et les affiches en forme de courbe
    """
    labels = [i+1 for i in range(len(D1))]
  
    x = np.arange(len(labels))  # the label locations
    width = 0.35  # the width of the bars

    fig, ax = plt.subplots()
    rects1 = ax.bar(x - width/2, D1, width, label='D1', color='blue')
    
    if D2!=0:
        rects2 = ax.bar(x + width/2, D2, width, label='D2', color='orange')

    ax.set_ylabel('Nb occurrences')
    #legend = ax.legend(loc='upper center', shadow=False, fontsize='x-large')
    legend = ax.legend()
    #ax.set_title('titre au besoin')
    plt.show()
    
def plot_epsilon(eps):
    """
        eps: [ [epsilon,occ1,occ2,output] ]
        result : the plot
    """
    # Plot curve: occ1,occ2,epsilon = f(output by epsilon desc)
    
    x=[]
    y1=[]
    y2=[]
    y3=[]
    step=0
    for i in eps: #epsilon, occ_D1, occ_D2, output
        x.append(step)
        y1.append(i[1]) #occ_D1
        y2.append(i[2]) #occ_D2
        y3.append(i[0]) #epsilon
        step=step+1

    # curve : occ1,occ2 = f(output by epsilon desc):
    plt.plot(x,y1,'r--')   # occ_D1
    plt.plot(x,y2,'b--')   # occ_D2
    plt.ylabel('Number of occurences')
    plt.xlabel('Disctinct outputs order by epsilon')
    plt2 = plt.twinx()
    plt2.plot(x,y3,'g--') # epsilon
    plt2.set_ylabel('$\epsilon$')
    plt2.grid()
    plt.savefig('Curves/experimental.pdf')  
    plt.savefig('Curves/experimental.png')  
    plt.show()



from math import exp

def plot_epsilon2(eps):
    """
        eps: [ [epsilon,occ1,occ2,output] ]
        result : the plot
    """
    # Plot curve: occ1,occ2,epsilon = f(output by epsilon desc)
    
    x=[]
    y1=[]
    y2=[]
    y3=[]
    step=0
    for i in eps: #epsilon, occ_D1, occ_D2, output
        x.append(step)
        y1.append(i[1]) #occ_D1
        y2.append(i[2]) #occ_D2
        y3.append(i[0]) #epsilon
        step=step+1

    # curve : occ1,occ2 = f(output by epsilon desc):
   
    plt.plot(x,y3,'g--') # epsilon
    plt.ylabel("Log of the probabilities' ratio")
    plt.xlabel('Distinct outputs ordered by ratio of probabilities')
    #plt.yscale('log')
    
    plt2 = plt.twinx()
    plt2.plot(x,y1,'r--')   # occ_D1
    plt2.plot(x,y2,'b--')   # occ_D2
    plt2.set_ylabel('Number of occurences')
    

    plt2.grid()
    
    plt.savefig('Curves/experimental2.pdf')  
    plt.savefig('Curves/experimental2.png')
    plt.savefig('Final/fig5.pdf')  
    plt.savefig('Final/fig5.png')  
    plt.show()    
    




def plot_delta(eps):
    """
        eps: [ [epsilon,occ1,occ2,output] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)

    produced_outputs=0
    for i in eps: #epsilon, occ_D1, occ_D2, output
        produced_outputs = i[1]+i[2] + produced_outputs
    
    
    x1=[]
    y3=[]
    cumul_proba=0
    for i in eps: #epsilon, occ_D1, occ_D2, output
        cumul_proba = (i[1]+i[2])/produced_outputs + cumul_proba
        x1.append(cumul_proba) #cumulated proba
        y3.append(i[0]) #epsilon

    # curve : epsilon = f(delta)
    plt.plot(x1,y3,'r--')   # occ_D1
    plt.ylabel('epsilon')
    plt.xlabel('delta')
    plt.ylim([0, 3])
    plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.show()
    
    
def plot_delta2(delta):
    """
        delta: [ [epsilon,delta] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)

    x1=[]
    y3=[]
    for i in delta: #epsilon, occ_D1, occ_D2, output
        x1.append(i[1]) #cumulated proba
        y3.append(i[0]) #epsilon

    # curve : epsilon = f(delta)
    plt.plot(x1,y3,'r--')   # occ_D1
    plt.ylabel('epsilon')
    plt.xlabel('delta')
    plt.ylim([0, 3])
    plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.show()

def plot_delta3(delta, delta2):
    """
        delta, delta2: [ [epsilon,delta] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)

    x1=[]
    y3=[]
    x2=[]
    y4=[]
    for i in delta: #epsilon, occ_D1, occ_D2, output
        x1.append(i[1]) #cumulated proba
        y3.append(i[0]) #epsilon
    for i in delta2: #epsilon, occ_D1, occ_D2, output
        x2.append(i[1]) #cumulated proba
        y4.append(i[0]) #epsilon

    # curve : epsilon = f(delta)
    plt.plot(x1,y3,'r--')
    plt.plot(x2,y4,'g--')
    plt.ylabel('epsilon')
    plt.xlabel('delta')
    plt.ylim([0, 3])
    plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.show()    
    
def plot_three_delta(delta, delta2, delta3):
    """
        delta, delta2: [ [epsilon,delta] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)

    x1=[]
    y3=[]
    x2=[]
    y4=[]
    x3=[]
    y5=[]
    for i in delta: #epsilon, occ_D1, occ_D2, output
        x1.append(i[1]) #cumulated proba
        y3.append(i[0]) #epsilon
    for i in delta2: #epsilon, occ_D1, occ_D2, output
        x2.append(i[1]) #cumulated proba
        y4.append(i[0]) #epsilon
    for i in delta3: #epsilon, occ_D1, occ_D2, output
        x3.append(i[1]) #cumulated proba
        y5.append(i[0]) #epsilon
    # curve : epsilon = f(delta)
    plt.plot(x1,y3,'r--')
    plt.plot(x2,y4,'g--')
    plt.plot(x3,y5,'b--')
    plt.ylabel('epsilon')
    plt.xlabel('delta')
    plt.ylim([0, 3])
    plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.show()    
    

    
def plot_birds(birds,lissage=1):
    recapture1=[]
    recapture2=[]
    nb_outputs1=[]
    nb_outputs2=[]
    for i in birds:

        if i[1]=='D1':
            recapture1.append(i[2])
            nb_outputs1.append(i[3])
        elif i[1]=='D2':
            recapture2.append(i[2])
            nb_outputs2.append(i[3])
    recapture_moyen=[]
    recapture_moyen2=[]
    for i in range(len(recapture1)-lissage):
        res=0
        for j in range(lissage):
            res=res+recapture1[i+j]
        recapture_moyen.append(res/lissage)


    for i in range(len(recapture2)-lissage):
        res=0
        for j in range(lissage):
            res=res+recapture2[i+j]
        recapture_moyen2.append(res/lissage)    

    plt.plot(nb_outputs1[lissage:],recapture_moyen)
    plt.plot(nb_outputs2[lissage:],recapture_moyen2)
    plt.yscale("log")

def plot_three_delta2(delta, delta2, delta3,filename):
    """
        delta, delta2: [ [epsilon,delta] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)
    filename='Pdf-comparison/'+filename
    x1=[]
    y3=[]
    x2=[]
    y4=[]
    x3=[]
    y5=[]
    for i in delta: #epsilon, occ_D1, occ_D2, output
        x1.append(i[1]) #cumulated proba
        y3.append(i[0]) #epsilon
    for i in delta2: #epsilon, occ_D1, occ_D2, output
        x2.append(i[1]) #cumulated proba
        y4.append(i[0]) #epsilon
    for i in delta3: #epsilon, occ_D1, occ_D2, output
        x3.append(i[1]) #cumulated proba
        y5.append(i[0]) #epsilon
    # curve : epsilon = f(delta)
    plt.plot(x1,y3,'r--',label="XP")
    plt.plot(x2,y4,'g--',label="TH")
    plt.plot(x3,y5,'b--',label="Semi-XP")
    plt.ylabel('epsilon')
    plt.xlabel('delta')
    #plt.ylim([0, 3])
    #plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.legend()
    plt.title(filename[15:-70])
    if path.exists('Pdf-comparison')==False:
        os.mkdir('Pdf-comparison')
    plt.savefig(filename)
    plt.show()
    

def plot_three_delta_inverse(delta, delta2, delta3):
    """
        delta, delta2: [ [epsilon,delta] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)

    x1=[]
    y3=[]
    x2=[]
    y4=[]
    x3=[]
    y5=[]
    for i in delta: #epsilon, occ_D1, occ_D2, output
        x1.append(i[1]) #cumulated proba
        y3.append(i[0]) #epsilon
    for i in delta2: #epsilon, occ_D1, occ_D2, output
        x2.append(i[1]) #cumulated proba
        y4.append(i[0]) #epsilon
    for i in delta3: #epsilon, occ_D1, occ_D2, output
        x3.append(i[1]) #cumulated proba
        y5.append(i[0]) #epsilon
    # curve : epsilon = f(delta)
    plt.plot(y3,x1,'r--',label="1er argument")
    plt.plot(y4,x2,'g--',label="2eme argument")
    plt.plot(y5,x3,'b--',label="3eme argument")
    plt.xlabel('epsilon')
    plt.ylabel('delta')
    #lt.ylim([0, 3])
    #plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.legend()
    plt.show()   
    
def plot_four_delta(delta, delta2, delta3,delta4,filename):
    """
        delta, delta2: [ [epsilon,delta] ]
        result : the plot
    """
    # Plot curve : epsilon = f(delta)
    filename='Pdf-comparison/FALSE_'+filename
    x1=[]
    y3=[]
    x2=[]
    y4=[]
    x3=[]
    y5=[]
    x4=[]
    y6=[]
    for i in delta: #epsilon, occ_D1, occ_D2, output
        x1.append(i[1]) #cumulated proba
        y3.append(i[0]) #epsilon
    for i in delta2: #epsilon, occ_D1, occ_D2, output
        x2.append(i[1]) #cumulated proba
        y4.append(i[0]) #epsilon
    for i in delta3: #epsilon, occ_D1, occ_D2, output
        x3.append(i[1]) #cumulated proba
        y5.append(i[0]) #epsilon
    for i in delta4: #epsilon, occ_D1, occ_D2, output
        x4.append(i[1]) #cumulated proba
        y6.append(i[0]) #epsilon
    # curve : epsilon = f(delta)
    plt.plot(x1,y3,'r--',label="XP")
    plt.plot(x2,y4,'g--',label="TH")
    plt.plot(x3,y5,'b--',label="Semi-XP")
    plt.plot(x4,y6,'y--',label="Semi-XP(false)")
    plt.ylabel('epsilon')
    plt.xlabel('delta')
    #plt.ylim([0, 3])
    #plt.yticks(np.arange(0, 3, 0.5))
    plt.grid()
    plt.legend()
    plt.title(filename[21:-70])
    if path.exists('Pdf-comparison')==False:
        os.mkdir('Pdf-comparison')
    plt.savefig(filename)
    plt.show()
        
def plot_birds2(birds,lissage=1,filename="test"):
    filename='Pdf-birds/'+filename
    recapture1=[]
    recapture2=[]
    nb_outputs1=[]
    nb_outputs2=[]
    for i in birds:

        if i[1]=='D1':
            recapture1.append(i[2])
            nb_outputs1.append(i[3])
        elif i[1]=='D2':
            recapture2.append(i[2])
            nb_outputs2.append(i[3])
    recapture_moyen=[]
    recapture_moyen2=[]
    for i in range(len(recapture1)-lissage):
        res=0
        for j in range(lissage):
            res=res+recapture1[i+j]
        recapture_moyen.append(res/lissage)


    for i in range(len(recapture2)-lissage):
        res=0
        for j in range(lissage):
            res=res+recapture2[i+j]
        recapture_moyen2.append(res/lissage)    

    plt.plot(nb_outputs1[lissage:],recapture_moyen, label="D1")
    plt.plot(nb_outputs2[lissage:],recapture_moyen2, label="D2")
    plt.yscale("log")
    plt.ylabel('% of new capture')
    plt.xlabel('number of outputs')
    plt.grid()
    plt.legend()
    plt.title(filename[10:-70])
    if path.exists('Pdf-birds')==False:
        os.mkdir('Pdf-birds')
    plt.savefig(filename)
    plt.show()
    
def plot_comparison_TH_Semi_TH_Buffer_increasing(tau,plie,d,delta,nb_runs=10,pas=0.1,nmax=500):
    if path.exists('Comparison-TH-VS-Semi_TH')==False:
        os.mkdir('Comparison-TH-VS-Semi_TH')
    x_coordinates=[]
    eps_TH=[]
    eps_Semi_TH=[]
    x_coordinates.append(2)
    eps_TH.append(eps_algo(tau,2,d, plie,delta))
    eps0=get_epsilon_Semi_TH(tau,plie,2,d,delta,nb_runs,pas)
    eps_Semi_TH.append(eps0)
    n=10
    while n <nmax:
        x_coordinates.append(n)
        eps_TH.append(eps_algo(tau,n,d, plie,delta))
        eps0=get_epsilon_Semi_TH(tau,plie,n,d,delta,nb_runs,pas,eps0)
        eps_Semi_TH.append(eps0)
        n=n+20
        
    plt.plot(x_coordinates, eps_TH, label="TH")
    plt.plot(x_coordinates, eps_Semi_TH, label="Semi-TH")
    plt.grid()
    plt.legend()
    plt.xlabel('Number of input data processed by the scrambler "$n$" ($\sigma$={})'.format(plie))
    plt.ylabel('$\\varepsilon$')
    filename="Comparison-TH-VS-Semi_TH/Eps_TH_VS_Semi_TH_Buffer_increasing,tau={},plie={},d={},delta={}.jpeg".format(tau,plie,d,delta)
    plt.savefig(filename)
    plt.show()
        

def plot_comparison_TH_Semi_TH_Buffer_increasing_delta_variable(tau,plie,d,nb_runs=10,pas=0.1,nmax=500):
    if path.exists('Comparison-TH-VS-Semi_TH')==False:
        os.mkdir('Comparison-TH-VS-Semi_TH')
    x_coordinates=[]
    eps_TH=[]
    eps_Semi_TH=[]
    x_coordinates.append(2)
    eps_TH.append(eps_algo(tau,2,d, plie,1/4))
    eps0=get_epsilon_Semi_TH(tau,plie,2,d,1/4,nb_runs,pas)
    eps_Semi_TH.append(eps0)
    n=10
    while n <nmax:
        delta=1/(n**2)
        x_coordinates.append(n)
        eps_TH.append(eps_algo(tau,n,d, plie,delta))
        eps0=get_epsilon_Semi_TH(tau,plie,n,d,delta,nb_runs,pas,eps0)
        eps_Semi_TH.append(eps0)
        n=n+20
        
    plt.plot(x_coordinates, eps_TH, label="TH")
    plt.plot(x_coordinates, eps_Semi_TH, label="Semi-TH")
    plt.grid()
    plt.legend()
    plt.xlabel('Number of input data processed by the scrambler "$n$" ($\sigma$={})'.format(plie))
    plt.ylabel('$\\varepsilon$')
    filename="Comparison-TH-VS-Semi_TH/Eps_TH_VS_Semi_TH_Buffer_increasing,tau={},plie={},d={},delta_variable.jpeg".format(tau,plie,d)
    plt.savefig(filename)
    plt.show()
def plot_selection(x_coordinates, y_coordinates_dict,legend):

    for y_coordinates in y_coordinates_dict:
        plt.plot(x_coordinates, y_coordinates_dict[y_coordinates], label=legend[y_coordinates])
    plt.legend()

    
    
def confidence_interval(x_coordinates,y_coordinates,confidence_rate=0.95):
    ci = confidence_rate * np.std(y_coordinates)/np.mean(y_coordinates)
    plt.scatter(x_coordinates,y_coordinates,s=5)
    plt.fill_between(x_coordinates, (y_coordinates-ci), (y_coordinates+ci), color='b', alpha=.2)
    