from math import ceil
def broadcast_messages(tau,N):
    """
        Tau: number of target
        N: number of useful input
        return: total number of messages for the broadcast
    """
    return tau*N

def broadcast_secure_channels(tau,N):
    """
        Tau: number of target
        N: number of useful input
        return: total number of secur channels for the broadcast
    """
    return tau*N

def number_of_messages_baseline(tau,N,d):
    """
        Tau: number of target
        N: number of useful input
        Plie: probability of lying
        d: number of dummies
        return: total number of messages for the baseline
    """
    return N*(d+1)


def number_of_secure_channels_baseline(tau,N,d):
    """
        Tau: number of target
        N: number of useful input
        Plie: probability of lying
        d: number of dummies
        return: number of remote attestation for the baseline
    """
    return N*(d+1)

def number_of_messages_GB(tau,S,n,d):
    """
        Tau: number of target
        N: number of useful input
        n: scramblers' buffer size
        Plie: probability of lying
        d: number of dummies
        return: number of messages for the Algo A_(n,d)
    """
    numberOfScramblers=ceil(S/n)
    #numberOfScramblers=scramblers
    NumberMessages=S+numberOfScramblers*(n+d)
    return NumberMessages

def number_of_messages_KM(tau,S,n,d,IT):
    """
        Tau: number of target
        N: number of useful input
        n: scramblers' buffer size
        Plie: probability of lying
        d: number of dummies
        return: number of messages for the Algo A_(n,d)
    """
    numberOfScramblers=ceil(S/n)
    #numberOfScramblers=scramblers
    NumberMessages=(S+numberOfScramblers*(n+d)+tau*numberOfScramblers+S)*IT
    return NumberMessages
def number_of_secure_channels_algo(tau,S,scramblers):
    """
        Tau: number of target
        N: number of useful input
        n: scramblers' buffer size
        Plie: probability of lying
        d: number of dummies
        return: number of remote attestation for the Algo A_(n,d)
    """
    #numberOfScramblers=ceil(N/n)
    numberOfScramblers=scramblers
    NumberRA=numberOfScramblers*(int(S/scramblers)+tau)
    return NumberRA

'''
def number_of_messages_algo(tau,N,n,d):
    """
        Tau: number of target
        N: number of useful input
        n: scramblers' buffer size
        Plie: probability of lying
        d: number of dummies
        return: number of messages for the Algo A_(n,d)
    """
    #numberOfScramblers=ceil(N/n)
    numberOfScramblers=N/n
    NumberMessages=N+numberOfScramblers*(n+d)
    return NumberMessages



def number_of_secure_channels_algo(tau,N,n,d):
    """
        Tau: number of target
        N: number of useful input
        n: scramblers' buffer size
        Plie: probability of lying
        d: number of dummies
        return: number of remote attestation for the Algo A_(n,d)
    """
    #numberOfScramblers=ceil(N/n)
    numberOfScramblers=N/n
    NumberRA=numberOfScramblers*(n+tau)
    return NumberRA
'''