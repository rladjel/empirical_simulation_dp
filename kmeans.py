import numpy as np
from numpy.linalg import norm
from sklearn.metrics import pairwise_distances_argmin
from sklearn.metrics import accuracy_score,mean_squared_error
    
    
def compute_centroids(n_clusters,X, y):
    ''' calcul des centroid des clusters '''
    centroids = np.zeros((n_clusters, X.shape[1]))
    for k in range(n_clusters):
        centroids[k, :] = np.mean(X[y == k, :], axis=0)
    return centroids

def score(predicted_centroids,real_centroids):
    list1=predicted_centroids.copy()
    list2=real_centroids.copy()
    list1.sort(axis=0)
    list2.sort(axis=0)
    return mean_squared_error(list1,list2)    
    
def init_clusters(X,n_clusters,random_seed):

    rng=np.random.RandomState(random_seed)
    n_samples = X.shape[0]
    seeds = rng.permutation(n_samples)[:n_clusters]
    centers = X[seeds]
    return centers
    
    
    
def sample(X, sampling):
    #here we sample
    random_idx = np.random.permutation(X.shape[0])
    number_outputs=int((1-sampling)*X.shape[0])
    sample = X[random_idx[:number_outputs]]
    #sample=X
    return sample





def KM(X, n_clusters, sampling=0, random_seed=2,nb_iterations=100):
    # cluster initialization
    
    centers = init_clusters(X,n_clusters,random_seed)
    #X_sampled=sample(X, sampling) #si on veut sampler une fois et garder toujours les mêmes données 
    iteration=0
    while True:
        # Assign labels based on closest center
        
        X_sampled=sample(X, sampling) #si on veut sampler à chaque fois
        
        labels = pairwise_distances_argmin(X_sampled, centers)
        
        # Find new centers from means of points
        new_centers = np.array([X_sampled[labels == i].mean(0)
                                for i in range(n_clusters)])
        
        # Check for convergence
        if np.all(centers == new_centers): # changer peut être cette égalité pour être plus souple 
            break
            
        iteration+=1
        if iteration>=nb_iterations:
            break
        centers = new_centers
    
    labels = pairwise_distances_argmin(X, centers)
    return centers, labels

