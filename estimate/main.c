#include <stdio.h>
#include <time.h>
#include <math.h>

double eps_baseline(long tau, long d_baseline, double p)
{

    double top_baseline = (1 - p) * (tau - d_baseline )* (tau - 1 );

    double bottom_baseline = (p) * (d_baseline + 1)* (tau - d_baseline -1);
    double eps_baseline = (top_baseline / bottom_baseline)+1;
    eps_baseline= log(eps_baseline);
    if (eps_baseline < 0)
    {
        eps_baseline = -eps_baseline;
    }

    return eps_baseline;
}





long long comb_log(long n, long k )
{
    double N=(double)n;
    double K=(double)k;
    long long  result = 0;
    for (long i = 1; i <= K; i++)
    {
        result += log(N - (K - i));
        result -= log(i);
    }
    return result;
}

double estimate(long tau, long d, double plie, double eps, long m, long R)
{

    long x1=0,x2=1, L=0,W;
    double l,iden;
    long equality1=0,equality2=0;
    for (long r=0; r<R;r++)
    {
           	
        W=0;
        iden=0;
        l=0;
        for (long i=0;i<m+d;i++)
        {	

            W = rand()%tau;
            if(W==x1)
            {
                equality1=1;
                equality2=0;
            }else if (W==x2)
            {
                equality1=0;
                equality2=1;
            }else
            {
                equality1=0;
                equality2=0;
            }
            iden=equality1-exp(eps)*equality2;
            l+= plie * (1 - exp(eps)) + (1 - plie) * tau * iden;

        }
        if (l>0)
        {
            L+= l;
        }

    }

    return (double)L/R;
}


double get_delta_semi_TH(long tau, double plie, long n, long d, double eps, long nb_runs)
{
    double result=0;
    double A,B,C,D,E,F;
    for (int m=1; m<=n;m++)
    {

        A=log(m)-log(m+d);
        B=comb_log(n, m);
        C=log(plie)*m;
        D=log(1-plie)*(n-m);


        E=estimate(tau, d, plie, eps, m, nb_runs);

        result+=exp(A+B+C+D)*E;

    }
    F=exp(-log(plie)-log(n));
    return result*F;
}

double get_epsilon_Semi_TH_optimized(long tau,double plie,long n, long d, double delta, long nb_runs)
{

    double EPS_MAX=eps_baseline(tau, 0, plie);
    long MAX_DICHOTOMIE=10;
    long size=pow(2, MAX_DICHOTOMIE);
    double epsilons[size], delta_tmp,closer_delta,mini=1;

    for (int i=0;i<size;i++)
    {

        epsilons[i]=i*EPS_MAX/size;
    }

    long indice=size/2;
    long pas=indice/2;
    long best_indice=0;

    for (int i=0;i<=MAX_DICHOTOMIE;i++)
    {
    	

        delta_tmp=get_delta_semi_TH(tau,plie,n,d,epsilons[indice],nb_runs);
        //printf("dico=%d\n",i);
        /*printf("Dico nb = %d\n",i);
        printf("delta_tmp = %lf\n",delta_tmp);
        printf("mini = %lf\n",mini);*/
        if (mini>fabs(delta_tmp-delta))
        {
            mini=fabs(delta_tmp-delta);
            closer_delta=delta_tmp;
            best_indice=indice;
        }

        if (delta_tmp>delta)
        {
            indice=indice+pas;
        }else if(delta_tmp<delta)
        {
            indice=indice-pas;
        }else
        {
            return epsilons[indice];
        }
        pas=pas/2;
    }
    return epsilons[best_indice];
}



long main()
{
    srand(time(NULL));   // Initialization, should only be called once.
    long tau,d,m,nb_runs,n;
    double plie,eps,delta,val;
    double time_spent = 0.0;
    delta=0.0001;
    tau=50;
    plie=0.1;
    eps=1;

    long ns[]={10,50,500};
    long ds[]={5,50,500};
    long nb_runss[]={10,100,1000};
    int nb_run_size=3;
    int nb_tries=5;

    //Estimate
    printf("*************************\n");
    printf("Computing estimates\n");
    printf("*************************\n");
    for (int j=0;j<nb_run_size;j++)
    {
        for(int k=0;k<=2;k++)
        {
            m=ds[k]/5;
            printf("*************************\n");
            printf("nb_runs=%ld, d=%ld,m=%ld \n",nb_runss[j],ds[k],m);
            printf("*************************\n");
            for (int i=0;i<nb_tries;i++)
            {
                clock_t begin = clock();
                val=estimate( tau,  ds[k],  plie,  eps,  m,  nb_runss[j]);
                clock_t end = clock();
                time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
                printf("Val = %lf, Time=%lf \n",val,time_spent);
            }

        }


    }



    //deltas
    printf("*************************\n");
    printf("Computing deltas\n");
    printf("*************************\n");
    for (int j=0;j<nb_run_size;j++)
    {
        for(int k=0;k<=2;k++)
        {
            d=ns[k]/2;
            printf("*************************\n");
            printf("nb_runs=%ld, n=%ld, d=%ld \n",nb_runss[j],ns[k],d);
            printf("*************************\n");
            for (int i=0;i<nb_tries;i++)
            {
                clock_t begin = clock();
                val=get_delta_semi_TH( tau,  plie,  ns[k],  d,  eps,  nb_runss[j]);
                clock_t end = clock();
                time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
                printf("Val = %lf, Time=%lf \n",val,time_spent);
            }

        }


    }

    //epsilons
    printf("*************************\n");
    printf("Computing epsilons\n");
    printf("*************************\n");
    for (int j=0;j<nb_run_size;j++)
    {
        for(int k=0;k<=2;k++)
        {
            d=ns[k]/2;
            printf("*************************\n");
            printf("nb_runs=%ld, n=%ld, d=%ld \n",nb_runss[j],ns[k],d);
            printf("*************************\n");
            for (int i=0;i<nb_tries;i++)
            {
                clock_t begin = clock();
                val=get_epsilon_Semi_TH_optimized(tau,plie,ns[k], d, delta, nb_runss[j]);
                clock_t end = clock();
                time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
                printf("Val = %lf, Time=%lf \n",val,time_spent);
            }

        }


    }


    return 0;
}
